# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/danyalejandro/collisions/src/app/App.cpp" "/Users/danyalejandro/collisions/cmake-build-debug/CMakeFiles/ogre3d-collisions.dir/src/app/App.cpp.o"
  "/Users/danyalejandro/collisions/src/app/CamGuy.cpp" "/Users/danyalejandro/collisions/cmake-build-debug/CMakeFiles/ogre3d-collisions.dir/src/app/CamGuy.cpp.o"
  "/Users/danyalejandro/collisions/src/collider/ColCuboid.cpp" "/Users/danyalejandro/collisions/cmake-build-debug/CMakeFiles/ogre3d-collisions.dir/src/collider/ColCuboid.cpp.o"
  "/Users/danyalejandro/collisions/src/collider/Collider.cpp" "/Users/danyalejandro/collisions/cmake-build-debug/CMakeFiles/ogre3d-collisions.dir/src/collider/Collider.cpp.o"
  "/Users/danyalejandro/collisions/src/collider/Edge.cpp" "/Users/danyalejandro/collisions/cmake-build-debug/CMakeFiles/ogre3d-collisions.dir/src/collider/Edge.cpp.o"
  "/Users/danyalejandro/collisions/src/collider/Manager.cpp" "/Users/danyalejandro/collisions/cmake-build-debug/CMakeFiles/ogre3d-collisions.dir/src/collider/Manager.cpp.o"
  "/Users/danyalejandro/collisions/src/collider/Quad.cpp" "/Users/danyalejandro/collisions/cmake-build-debug/CMakeFiles/ogre3d-collisions.dir/src/collider/Quad.cpp.o"
  "/Users/danyalejandro/collisions/src/main.cpp" "/Users/danyalejandro/collisions/cmake-build-debug/CMakeFiles/ogre3d-collisions.dir/src/main.cpp.o"
  "/Users/danyalejandro/collisions/src/mesh/Cuboid.cpp" "/Users/danyalejandro/collisions/cmake-build-debug/CMakeFiles/ogre3d-collisions.dir/src/mesh/Cuboid.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "AppleClang")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/Users/danyalejandro/ogre/build/sdk/include/OGRE"
  "/Users/danyalejandro/ogre/build/sdk/include/OGRE/OSX"
  "/Users/danyalejandro/ogre/build/sdk/include/OGRE/Bites"
  "/Users/danyalejandro/ogre/build/sdk/include/OGRE/Overlay"
  "/Users/danyalejandro/ogre/build/sdk/include/OGRE/RTShaderSystem"
  "/usr/local/include"
  "/Library/Frameworks/SDL2.framework/Versions/A/Headers"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
